import requests

base_url = "https://integration-skill-test-server.herokuapp.com/"


def get_credentials_from_txt() -> str:
    try:
        with open("../../input_files/token.txt") as f:
            lines = f.readlines()
        return lines[0]
    except Exception:
        raise Exception("no token.txt file found on input_files folder")


# def get_credentials() -> str:
#     # Initial Setup
#     client_id = "mRkZGFjM"
#     client_secret = "ZGVmMjMz"
#     grant_type = "client_credentials"
#     url = base_url + f"/oauth/token?client_id={client_id}&client_secret={client_secret}&grant_type={grant_type}"
#     # Request
#     response = requests.post(url)
#     return response.json()["access_token"]


def get_store_id(token: str, merchant_name: str) -> str:
    # Initial Setup
    url = base_url + "/api/merchants"
    payload = {}
    headers = {"token": f"Bearer {token}"}
    # Request
    response = requests.get(url, headers=headers)
    # We look for Richard's
    store_id = ""
    for item in response.json()["merchants"]:
        if item["name"] == merchant_name:
            store_id = item["id"]

    return store_id


def update_store(token: str, merchant_id: str):
    # Initial Setup
    url = base_url + f"/api/merchants/{merchant_id}"
    headers = {"token": f"Bearer {token}"}
    payload = {
        "can_be_deleted": True,
        "can_be_updated": True,
        "id": merchant_id,
        "is_active": True,
        "name": "test",
    }
    # Request
    response = requests.put(url, json=payload, headers=headers)
    return response


def delete_store(token: str, merchant_id: str):
    # Initial Setup
    url = base_url + f"/api/merchants/{merchant_id}"
    headers = {"token": f"Bearer {token}"}
    # Request
    response = requests.delete(url, headers=headers)
    return response


def send_product(
    token: str,
    merchant_id: str,
    sku: str,
    bar_codes,
    brand_name,
    item_name,
    item_description,
    package,
    image_url,
    category,
    url,
    branch,
    stock: int,
    price: float,
):
    # Initial Setup
    headers = {"token": f"Bearer {token}"}
    payload = {
        "merchant_id": merchant_id,
        "sku": sku,
        "barcodes": [bar_codes],
        "brand": brand_name,
        "name": item_name,
        "description": item_description,
        "package": package,
        "image_url": image_url,
        "category": category,
        "url": url,
        "branch_products": [
            {"branch": branch, "stock": stock, "price": price},
            # {"branch": "Heartland Supercentre", "stock": -123, "price": 12.34},
        ],
    }
    # Request
    response = requests.post(url, json=payload, headers=headers)
    return response
