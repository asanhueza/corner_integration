import pandas as pd


def process_products_file(csv_file: str):
    # Cleansing ...
    input_csv = pd.read_csv(f"../../input_files/{csv_file}", sep="|")  # load file
    clean_input = input_csv.drop_duplicates(keep="first")  # remove duplicates

    sku_products = clean_input["SKU"].drop_duplicates()  # check for SKU duplicates
    if sku_products.shape[0] != clean_input.shape[0]:  # if sizes differ, there were SKU dropped duplicates
        raise Exception(
            f"Debug --> {csv_file} file has no unique SKU numbers ..."
        )  # We assume this cant happen, so we raise

    # Filtering ...
    column_types = clean_input.dtypes  # First we detect which columns are 'object' type
    str_columns = []
    for item in column_types.iteritems():
        if item[1] == "object":
            str_columns.append(item[0])

    # Now, for each str-type column, we perform some general cleaning
    for column in str_columns:
        clean_input[column] = clean_input[column].str.strip()  # remove trailing/leading empty spaces
        clean_input[column] = clean_input[column].str.normalize("NFKD")  # remove and replace special characters
        clean_input[column] = clean_input[column].str.replace("<p>", "", regex=True)  # remove html tags
        clean_input[column] = clean_input[column].str.replace("</p>", "", regex=True)  # remove html tags

    # Special Handling ...
    new_column = (
        clean_input["CATEGORY"] + " | " + clean_input["SUB_CATEGORY"] + " | " + clean_input["SUB_SUB_CATEGORY"]
    ).str.lower()
    # we drop the older category info
    clean_input = clean_input.drop(columns=["CATEGORY", "SUB_CATEGORY", "SUB_SUB_CATEGORY"])
    # we add the new column
    clean_input.insert(loc=clean_input.shape[1], column="CATEGORY", value=new_column)
    return clean_input


def process_prices_file(csv_file: str):
    # Cleansing ...
    input_csv = pd.read_csv(f"../../input_files/{csv_file}", sep="|")  # load file
    clean_input = input_csv.drop_duplicates(keep="first")  # remove duplicates
    # First we detect which columns are 'object' type
    column_types = clean_input.dtypes
    str_columns = []
    for item in column_types.iteritems():
        if item[1] == "object":
            str_columns.append(item[0])

    # Now, for each str-type column, we perform some general cleaning
    for column in str_columns:
        clean_input[column] = clean_input[column].str.strip()  # remove trailing/leading empty spaces
        clean_input[column] = clean_input[column].str.normalize("NFKD")  # remove and replace special characters

    #  Filtering ...
    clean_input = clean_input[(clean_input.STOCK > 0) & ((clean_input.BRANCH == "RHSM") | (clean_input.BRANCH == "MM"))]
    # If there are rows with same SKU and BRANCH (only these columns duplicated), we assume the information is corrupted
    # so we only keep the items that are unique combination of SKU and BRANCH.
    boolean_series = clean_input[["SKU", "BRANCH"]].duplicated(keep=False)  # we mark every occurrence as False
    clean_input = clean_input[boolean_series]
    print(
        f"Debug --> Dropped {sum(boolean_series)} items from PRICES_STOCK, as their SKU/BRANCH combination is not unique ..."
    )
    # Personal note: When facing this dilemma, one could also resolve using some condition over the PRICE/STOCK info,
    # but its unclear which condition to apply (¿we privilege stock or price point?), so I just preferred to simply
    # discard the items, as the information sent maybe was erroneous ... hey maybe it could be not item at all! =(

    return clean_input
    # duplicated 26468 SKU with different branch --> for testing
    # duplicated 296517 SKU with same branch --> for testing


def process_merged_file(merged_file: pd) -> pd:
    # Two Criteria for cleaning merged file
    # 1.- After merging, some products (SKU) may not have been paired with some prices, resulting in empty
    #     BRANCH/STOCK/PRICE fields on the merged file. So we remove them.
    # 2.- If any row doesn't have info needed for the injection API, the information is incomplete, so we remove it

    # Now we remove any row that has nan in the needed fields for the injection API
    boolean_matrix = ~merged_file[
        [
            "SKU",  # 2nd criteria
            "CATEGORY",  # 2nd criteria
            "EAN",  # 2nd criteria
            "BRAND_NAME",  # 2nd criteria
            "ITEM_NAME",  # 2nd criteria
            "ITEM_DESCRIPTION",  # 2nd criteria
            "ITEM_IMG",  # 2nd criteria
            "BRANCH",  # 1st & 2nd criteria
            "STOCK",  # 1st & 2nd criteria
            "PRICE",  # 1st & 2nd criteria
        ]
    ].isna()
    boolean_series = boolean_matrix.all(axis=1)  # we perform AND operation per row
    print(f"Debug --> Dropped {sum(~boolean_series)} elements from merged file...")
    clean_merged = merged_file[boolean_series]
    return clean_merged


def find_packaging(item: str) -> str:
    # We insert all the possible units for packaging (based on inspection)
    units = [
        "GRS",
        "GRS.",
        "GR",
        "GR.",
        "G",
        "G.",
        "KG",
        "KG.",
        "KGS",
        "KGS.",
        "ML",
        "ML.",
        "LT",
        "LT.",
        "PZA",
        "PZA.",
        "UN",
        "UN.",
    ]
    try:
        for enum, substring in enumerate(item.split()):  # We split item by ' ' in substrings
            # When value is separated by ' ' from unit, for instance 100 GR
            if substring.isdigit():
                value = substring
                if item.split()[enum + 1] in units:  # If the next substring is a valid unit, then we return
                    unit = item.split()[enum + 1]
                    return value + " " + unit
            # When value is together with unit, for instance 1UN
            if any(char.isdigit() for char in substring):  # We look for numbers in the substring first
                for unit in units:
                    if unit in substring:
                        index = substring.find(unit)
                        value = substring[:index]
                        if value.isdigit():
                            return value + " " + unit

        return ""  # if no pattern were found, then we return an empty string on packaging

    except IndexError:  # if we got out of bounds (a final number without unit for instace)
        return ""
