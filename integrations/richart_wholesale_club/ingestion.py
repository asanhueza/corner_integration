from csv_utils import process_products_file, process_prices_file, process_merged_file, find_packaging
from api_utils import (
    get_credentials_from_txt,
    get_store_id,
    update_store,
    delete_store,
    send_product,
)
import pandas as pd


if __name__ == "__main__":
    try:
        # --> CLEANING & FILTERING & MERGING
        print("Debug --> Started Cleaning & Filtering files ...")
        clean_products = process_products_file("PRODUCTS.CSV")
        clean_prices = process_prices_file("PRICES-STOCK.CSV")
        print("Debug --> Started Merging PRODUCTS and PRICES-STOCK files ...")
        merged_csv = clean_products.merge(clean_prices, how="left", on="SKU")  # we merge data
        clean_merged = process_merged_file(merged_csv)  # we clean the merged data

        # --> 100 MOST EXPENSIVE ITEMS SELECTION
        print("Debug --> Selecting 100 most expensive items of RHSM and MM branches")
        RHSM = clean_merged[clean_merged["BRANCH"].isin(["RHSM"])]  # only RHSM branch items
        MM = clean_merged[clean_merged["BRANCH"].isin(["MM"])]  # only MM branch items

        sMM = MM.sort_values(by=["PRICE"], ascending=False).head(100)  # price sorting and 100 first items
        sRHSM = RHSM.sort_values(by=["PRICE"], ascending=False).head(100)  # price sorting and 100 first items

        # Personal note: There are > 200 items with the maximum price (90.3), so the question was ... how do we choose
        # those 200 items if the price criteria is already met? Truth is.. i was not able to find a fair condition, so
        # I just simply picked the first items.

        # Another approach could have chosen if their package info is available, or if they are an ORGANIC ITEM ...
        # but those conditions seems fairly random (and personal), so without more knowledge on how is the data
        # processed afterwards I was pretty blind on this decision.

        # Hey, its not the best, but it works =)

        concat_csv = pd.concat([sMM, sRHSM], ignore_index=True)  # Now we concatenate the file

        # --> API INSTRUCTIONS
        token = get_credentials_from_txt()  # we get the token from a txt file (must fill it)
        # token = get_credentials()  # we get the token
        richards_id = get_store_id(token, "Richard's")  # we get richard's id
        update_response = update_store(token, richards_id)  # we update richard's store
        beauty_id = get_store_id(token, "Beauty")  # we get beauty's id
        delete_response = delete_store(token, beauty_id)  # we delete beauty store

        # --> API PRODUCT INJECTION
        print("Debug --> Injecting products to API ...")
        concat_dict = concat_csv.to_dict("records")  # we iterate over a dict, not pd, in order to speed up the process
        for idx, item in enumerate(concat_dict):
            print(f"Debug --> Sending product data number {idx} of 200...")
            package = find_packaging(item["ITEM_DESCRIPTION"])  # we look for packaging info
            response = send_product(
                token=token,
                merchant_id=richards_id,
                sku=str(item["SKU"]),
                bar_codes=str(item["EAN"]),
                brand_name=item["BRAND_NAME"],
                item_name=item["ITEM_NAME"],
                item_description=item["ITEM_DESCRIPTION"],
                package=package,
                image_url=item["ITEM_IMG"],
                category=item["CATEGORY"],
                url="hola",
                branch=item["BRANCH"],
                stock=int(item["STOCK"]),
                price=float(item["PRICE"]),
            )
            # if we fail to ingest, then we make a warning and continue with the next item
            if response.status_code != 200:
                print(f"the index {idx} item {item} couldn't be ingested --> {response.json()}")
                continue

        print("Debug --> Successfully sent 200 Products to ingestion API !!!")

    except Exception as e:
        print(f"Debug --> Error while loading/cleaning .csv files --> {e}")
